---
layout: page
title: About
permalink: /about/
---

The idea behind this site is a 100% open source wiki-like repository for
greybearded sysadmins to share their knowledge and experience with the
up and coming youth.  It will be a collection of articles, stories, and 
lessons learned through the years of real world industry experiences.

All contributions to the site are welcome at the [public repository](https://gitlab.com/greybeardit/greybeardit.org).

[Original HN Post](https://news.ycombinator.com/item?id=33204579#33208982)
