---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Stories

{% for story in site.story %}
  [{{ story.title }}]({{ story.url }})
{% endfor %}

